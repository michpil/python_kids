import keyboard as kb
from time import sleep
import pytest
import importlib

# Focus on App
sleep(3)


@pytest.fixture(scope='function')
def get_sprite_state():
    object_state = importlib.import_module('object_state')
    object_state = importlib.reload(object_state)
    return object_state.sprite_state


@pytest.fixture()
def move_sprite_to_center():
    kb.send('space')
    sleep(3)


@pytest.fixture()
def move_sprite_2_steps_up():
    kb.send('w')
    sleep(1)
    kb.send('w')
    sleep(3)


@pytest.fixture()
def increase_sprite_size_twice():
    kb.send('c')
    sleep(1)
    kb.send('c')
    sleep(3)


@pytest.fixture()
def hide_sprite():
    kb.send('q')
    sleep(3)


def test_sprite_centered(move_sprite_to_center, get_sprite_state):
    assert get_sprite_state.get('position') == (240.0, 180.0), 'Wrong Coords'


def test_sprite_2_steps_up(move_sprite_2_steps_up, get_sprite_state):
    assert get_sprite_state.get('position') == (240.0, 180.0 - 20.0), 'Wrong Coords'


def test_sprite_increased_size_twice(increase_sprite_size_twice, get_sprite_state):
    assert get_sprite_state.get('size') == 140, 'Wrong Size'


def test_sprite_is_not_visible(hide_sprite, get_sprite_state):
    assert get_sprite_state.get('visible') is False, 'Sprite is visible'
