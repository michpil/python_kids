from time import sleep


def get_sprite_state(sprite):
    sprite_state = {'name': sprite._core.name, 'visible': sprite._core.visible, 'position': (sprite._core._pos.x, sprite._core._pos.y), 'size': sprite._core.size, 'direction': sprite._core._direction}
    with open('..\\tests\\object_state.py', 'w') as file:
        file.write('sprite_state = ' + str(sprite_state))
    print(sprite_state)
    sleep(0.1)
    return sprite_state
