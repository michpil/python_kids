# cat_sample (pyStage, converted from Scratch 3)

from pystage.en import Sprite, Stage
from tests.utils import get_sprite_state

stage = Stage()
stage.add_backdrop('blue_sky')
cat = stage.add_a_sprite(None)
cat.set_name("Cat")
cat.set_x(-182)
cat.set_y(-95)
# cat.go_to_back_layer()
# cat.go_forward(1)
cat.add_costume('cat_a', center_x=48, center_y=50)
cat.add_costume('cat_b', center_x=46, center_y=53)
cat.add_sound('meow')

def when_key_pressed_22(self):
    self.turn_right(45.0)

cat.when_key_pressed("r", when_key_pressed_22)

def when_key_pressed_1(self):
    self.hide()

cat.when_key_pressed("q", when_key_pressed_1)

def when_key_pressed_2(self):
    self.change_y_by(10.0)

cat.when_key_pressed("w", when_key_pressed_2)

def when_key_pressed_3(self):
    self.show()

cat.when_key_pressed("e", when_key_pressed_3)

def when_key_pressed_4(self):
    self.change_x_by(-10.0)

cat.when_key_pressed("a", when_key_pressed_4)

def when_key_pressed_5(self):
    self.change_y_by(-10.0)

cat.when_key_pressed("s", when_key_pressed_5)

def when_key_pressed_6(self):
    self.change_x_by(10.0)

cat.when_key_pressed("d", when_key_pressed_6)

def when_key_pressed_7(self):
    self.change_size_by(-20.0)

cat.when_key_pressed("z", when_key_pressed_7)

def when_key_pressed_8(self):
    self.change_size_by(20.0)

cat.when_key_pressed("c", when_key_pressed_8)

def when_key_pressed_9(self):
    self.set_y(0.0)
    self.set_x(0.0)
    self.start_sound("meow")

cat.when_key_pressed("space", when_key_pressed_9)

def when_program_starts_10(self):
    for num in range(5):
        self.wait(1)
        self.turn_right(45.0)
    cat.switch_costume_to('cat_b')
    while True:
        get_sprite_state(self)
        if self.touching_edge():
            self.think("Living on the Edge")

        sprite_state = get_sprite_state(self)
        if sprite_state.get('visible') is True:
            self.say('I am visible')
            self.wait(1)
            self.say('')

cat.when_program_starts(when_program_starts_10)

stage.play()
